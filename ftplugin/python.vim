" Indentation settings
:set expandtab
:set tabstop=4
:set shiftwidth=4

" Code folding
:set foldmethod=indent
nnoremap <space> za
vnoremap <space> zf
map zu :set foldlevel=20<CR>
map z0 :set foldlevel=0<CR>
map z1 :set foldlevel=1<CR>
map z2 :set foldlevel=2<CR>
map z3 :set foldlevel=3<CR>
:set indentexpr=

" Tab completion
"set omnifunc=pythoncomplete#Complete
"let g:SuperTabDefaultCompletionType = "context"
"set completeopt=menuone,longest,preview
