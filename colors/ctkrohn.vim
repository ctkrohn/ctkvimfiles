"--------------------------------------------------------------------
" Name Of File: ctkrohn.vim.
" Description: Gvim colorscheme, works best with version 6.1 GUI .
" Maintainer: Peter B�ckstr�m.
" Creator: Peter B�ckstr�m.
" URL: http://www.brookstream.org (Swedish).
" Credits: Inspiration from the darkdot scheme.
" Last Change: Friday, April 13, 2003.
" Installation: Drop this file in your $VIMRUNTIME/colors/ directory.
"--------------------------------------------------------------------

set background=dark
hi clear
if exists("syntax_on")
    syntax reset
endif
let g:colors_name="ctkrohn"

"--------------------------------------------------------------------

" Light blue, e.g. line numbers: #4682b4
" Medium gray, e.g. for cursor line: #202020

hi Normal		gui=none	guibg=#000000	guifg=#bbbbbb
hi Cursor				guibg=#44ff44   guifg=#000000
hi CursorLine 				guibg=#202020
hi ColorColumn 				guibg=#202020
hi Directory						guifg=#44ffff
hi DiffAdd				guibg=#080808	guifg=#ffff00
hi DiffDelete				guibg=#080808	guifg=#444444
hi DiffChange				guibg=#080808	guifg=#ffffff
hi DiffText				guibg=#080808	guifg=#bb0000
hi ErrorMsg				guibg=#880000	guifg=#ffffff
hi Folded				guibg=#000000   guifg=#cc0000
hi IncSearch				guibg=#000000	guifg=#bbcccc
hi LineNr				guibg=#000000	guifg=#4682b4
hi ModeMsg						guifg=#ffffff
hi MoreMsg			 			guifg=#44ff44
hi NonText						guifg=#4444ff
hi Question						guifg=#ffff00
hi SpecialKey						guifg=#4444ff

" Vertical split: disable, because we have line numbers
hi VertSplit				guibg=#000000   guifg=#000000

" Status line of active/inactive window
hi StatusLine		gui=bold	guibg=#4682b4	guifg=#000000
hi StatusLineNC		gui=bold	guibg=#202020	guifg=#ffffff		

hi Title						guifg=#ffffff
hi Visual		gui=none	guibg=#bbbbbb	guifg=#000000
hi WarningMsg						guifg=#ffff00


" syntax highlighting groups ----------------------------------------
hi Comment		gui=italic			guifg=#696969
hi Constant						guifg=#00aaaa
hi Identifier						guifg=#00e5ee
hi Statement 						guifg=#00ffff
hi PreProc						guifg=#8470ff
hi Type							guifg=#ffffff
hi Special		gui=none			guifg=#87cefa
hi Underlined 		gui=bold   			guifg=#4444ff
hi Ignore		 				guifg=#444444
hi Error				guibg=#000000	guifg=#bb0000
hi Todo					guibg=#aa0006	guifg=#fff300
hi Operator 		gui=none 			guifg=#00bfff
hi Function 	 				        guifg=#1e90ff
hi String 		gui=None 			guifg=#4682b4
hi Boolean						guifg=#9bcd9b

" Tabs
hi TabLineFill 		gui=None        guibg=#4682b4   guifg=#4682b4
hi TabLine              gui=bold        guibg=#4682b4   guifg=#202020
hi TabLineSel           gui=bold        guibg=#bbbbbb   guifg=#202020

"hi link Character Constant
"hi link Number    Constant
"hi link Boolean   Constant
"hi link Float   Number
"hi link Conditional Statement
"hi link Label   Statement
"hi link Keyword   Statement
"hi link Exception Statement
"hi link Repeat    Statement
"hi link Include   PreProc
"hi link Define    PreProc
"hi link Macro   PreProc
"hi link PreCondit PreProc
"hi link StorageClass  Type
"hi link Structure Type
"hi link Typedef   Type
"hi link Tag   Special
"hi link Delimiter Special
"hi link SpecialComment  Special
"hi link Debug   Special
"hi link FoldColumn  Folded




"- end of colorscheme -----------------------------------------------	
